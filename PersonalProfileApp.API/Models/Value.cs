namespace PersonalProfileApp.API.Models
{    public class Value
    {
        public int Id {get; set;}
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }
}