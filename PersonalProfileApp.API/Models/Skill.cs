namespace PersonalProfileApp.API.Models
{
    public class Skill
    {
        public string skillName { get; set; }
        public string skillDescription { get; set; }

        public long skillPercentage {get; set;}
    }
}