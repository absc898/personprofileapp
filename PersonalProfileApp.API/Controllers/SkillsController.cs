using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nest;
using PersonalProfileApp.API.Models;

namespace PersonalProfileApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillsController : ControllerBase
    {
        private ElasticClient client;

        public SkillsController()
        {
            var node = new Uri("http://localhost:9200");
            var settings = new ConnectionSettings();
            settings.DefaultIndex("resume");
            client = new ElasticClient(settings);
            
        }

        // GET api/skills
        [HttpGet]
        public async Task<IActionResult> GetAllSkills()
        {
            var searchResults = client.Search<Skill>(s=>s
                                .From(0)
                                .Size(100)
                                .Query(q=>q.MatchAll()
                                )
                            );

            foreach(var result in searchResults.Documents)
            {
                Console.WriteLine(result.skillName);
            }
            return Ok(searchResults.Documents);
        }

        [HttpGet("{skillName}")]
        public async Task<IActionResult> GetSkillbyName(string skillName)
        {
            var searchResults = client.Search<Skill>(s => s
                                            .Query(q => q
                                                .Match(m => m
                                                    .Field(f => f.skillName)
                                                    .Query(skillName))));

            if(searchResults.Hits.Count <= 0)
            {
                return Ok(this.GetAllSkills());
            }
            else
            {
                return Ok(searchResults.Documents);
            }

        }
    }
}