import { Component, OnInit } from "@angular/core";
import {
  transition,
  trigger,
  style,
  animate,
  state
} from "@angular/animations";

export const fadeAnimation = trigger("fadeAnimation", [
  transition(":enter", [
    style({ opacity: 0 }),
    animate("4000ms", style({ opacity: 1 }))
  ]),
  transition(":leave", [
    style({ opacity: 1 }),
    animate("300ms", style({ opacity: 0 }))
  ])
]);

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
  animations: [fadeAnimation]
})
export class HomeComponent implements OnInit {
  isShow: boolean;
  topPosToStartShowing = 100;
  isValue = false;
  isName = false;

  constructor() {}

  ngOnInit() {}

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  public executeSelectedChange = (event: any) => {
    console.log(event);
  };

  async gotoAboutMe() {
    document.querySelector("mat-sidenav-content").scroll({
      top: 800,
      left: 0,
      behavior: "smooth"
    });

    await this.sleep(500);
    this.isValue = true;
  }
}
