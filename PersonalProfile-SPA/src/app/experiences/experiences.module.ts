import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExperiencesComponent } from './experiences.component';
import { ExperiencesRoutingModule } from './experiences-routing/experiences-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ExperiencesRoutingModule,
    MaterialModule,
  ],
  declarations: [ExperiencesComponent]
})
export class ExperiencesModule { }
