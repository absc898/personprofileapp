import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { ProjectsComponent } from './projects.component';
import { ProjectsRoutingModule } from './projects-routing/projects-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    MaterialModule
  ],
  declarations: [] // inside of this should be ProjectComponent I though - ALALAL
})
export class ProjectsModule { }
