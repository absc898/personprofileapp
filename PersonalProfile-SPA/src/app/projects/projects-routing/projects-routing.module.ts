import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from '../projects.component';


const routes: Routes = [
  { path: 'projects', component: ProjectsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [ProjectsComponent]
})
export class ProjectsRoutingModule { }
