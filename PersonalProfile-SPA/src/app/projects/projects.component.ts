import { Component, OnInit, Inject, Input } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
//import { AndroidProjectComponent } from '../dialogs/androidProject/androidProject.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  animal: string;
  name: string;
  @Input() completedParam;


  constructor(public dialog: MatDialog) {}

  ngOnInit() {
  }

  openDialog(dataIn: any): void {

    let dialogRef;
    switch (dataIn) {
      case 'android':
        dialogRef = this.dialog.open(AndroidDialog, {
          width: '700px',
          height: '500px',
          data: dataIn
        });
        break;
      case 'beyond':
        dialogRef = this.dialog.open(BeyondDialog, {
          width: '690px',
          height: '450px',
          data: dataIn
        });
        break;
      case 'cloud':
          dialogRef = this.dialog.open(CloudDialog, {
            width: '700px',
            height: '600px',
            data: dataIn
          });
          break;
      case 'personal':
            dialogRef = this.dialog.open(PersonalDialog, {
              width: '750px',
              height: '550px',
              data: dataIn
            });
            break;
    }


    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

}

//////////// Dialogs for each project  //////

@Component({
  selector: 'app-androidProject',
  templateUrl: '../dialogs/androidProject/androidProject.component.html',
  styleUrls: ['../dialogs/androidProject/androidProject.component.css']
})
export class AndroidDialog {

  constructor(
    public dialogRef: MatDialogRef<AndroidDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      console.log(data);
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

//////////////////////////////////////////

@Component({
  selector: 'app-visaProject',
  templateUrl: '../dialogs/visaProject/visaProject.component.html',
  styleUrls: ['../dialogs/visaProject/visaProject.component.css']
})
export class BeyondDialog {

  constructor(
    public dialogRef: MatDialogRef<BeyondDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

///////////////////////////////////////

@Component({
  selector: 'app-cloudProject',
  templateUrl: '../dialogs/cloudProject/cloudProject.component.html',
  styleUrls: ['../dialogs/cloudProject/cloudProject.component.css']
})
export class CloudDialog {

  constructor(
    public dialogRef: MatDialogRef<CloudDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
}


////////////////////////////////////

@Component({
  selector: 'app-cloudProject',
  templateUrl: '../dialogs/PersonalProject/PersonalProject.component.html',
  styleUrls: ['../dialogs/PersonalProject/PersonalProject.component.css']
})
export class PersonalDialog {

  constructor(
    public dialogRef: MatDialogRef<CloudDialog>,
    @Inject(MAT_DIALOG_DATA) public data: PersonalDialog) {
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

