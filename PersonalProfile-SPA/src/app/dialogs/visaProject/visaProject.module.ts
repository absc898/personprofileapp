import {NgModule} from '@angular/core';
import {YouTubePlayerModule} from '@angular/youtube-player';

import { VisaProjectComponent } from './visaProject.component';

@NgModule({
  imports: [
    YouTubePlayerModule,
  ],
  declarations: [VisaProjectComponent],
  exports: [VisaProjectComponent]
})

export class VisaProjectModule {

}