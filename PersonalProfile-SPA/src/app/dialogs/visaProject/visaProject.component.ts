import { Component, OnInit, NgModule } from '@angular/core';

// @Component({
//   selector: 'app-visaProject',
//   templateUrl: './visaProject.component.html',
//   styleUrls: ['./visaProject.component.css']
// })

@Component({
  template: '<youtube-player videoId="BG4h6XC0l8Q"></youtube-player>',
  selector: 'app-video'
})

export class VisaProjectComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const tag = document.createElement('script');

    tag.src = 'https://www.youtube.com/iframe_api';
    document.body.appendChild(tag);
  }

}
