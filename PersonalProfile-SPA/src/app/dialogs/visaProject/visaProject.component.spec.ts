/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { VisaProjectComponent } from './visaProject.component';

describe('VisaProjectComponent', () => {
  let component: VisaProjectComponent;
  let fixture: ComponentFixture<VisaProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisaProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisaProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
