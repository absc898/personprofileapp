import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import {MatDialogModule} from '@angular/material/dialog';

import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { ValueComponent } from './value/value.component';
import { HomeComponent } from './home/home.component';
import { RoutingModule } from './routing/routing.module';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/header/sidenav-list/sidenav-list.component';
import { NgxSkillBarModule } from 'ngx-skill-bar';
import { AndroidDialog, BeyondDialog, CloudDialog, PersonalDialog } from './projects/projects.component';
import { VisaProjectModule } from './dialogs/visaProject/visaProject.module';

@NgModule({
   declarations: [
      AppComponent,
      LayoutComponent,
      ValueComponent,
      HomeComponent,
      HeaderComponent,
      SidenavListComponent,
      AndroidDialog,
      BeyondDialog,
      CloudDialog,
      PersonalDialog
   ],
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      HttpClientModule,
      MaterialModule,
      RoutingModule,
      NgxSkillBarModule,
      MatDialogModule,
      VisaProjectModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ],
   entryComponents: [AndroidDialog, BeyondDialog, CloudDialog, PersonalDialog]
})
export class AppModule { }
