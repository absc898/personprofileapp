import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'experiences', loadChildren: './../experiences/experiences.module#ExperiencesModule' },
  { path: 'projects', loadChildren: './../projects/projects.module#ProjectsModule' },
  { path: 'skills', loadChildren: './../skills/skills.module#SkillsModule' },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
