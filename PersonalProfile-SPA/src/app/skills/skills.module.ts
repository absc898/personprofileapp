import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkillsComponent } from './skills.component';
import { SkillsRoutingModule } from './skills-routing/skills-routing.module';
import { NgxSkillBarModule } from "ngx-skill-bar";

@NgModule({
  imports: [
    CommonModule,
    SkillsRoutingModule,
    MaterialModule,
    NgxSkillBarModule
  ],
  declarations: [SkillsComponent]
})
export class SkillsModule { }
