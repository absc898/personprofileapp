import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SkillsComponent } from '../skills.component';

const routes: Routes = [
    { path: 'skills', component: SkillsComponent }
  ];


@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(routes)
    ],
    exports: [
      RouterModule
    ],
    declarations: []
  })

  export class SkillsRoutingModule { }
