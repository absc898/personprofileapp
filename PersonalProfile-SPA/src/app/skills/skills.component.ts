import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchService } from '../search.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css'],
  providers: [SearchService]
})
export class SkillsComponent implements OnInit {
  skills: any;
  mResults: Object;
  searchTerm$ = new Subject<string>();
  isShow: boolean;
  topPosToStartShowing = 100;

  constructor(private http: HttpClient, private searchService: SearchService) { 
    this.searchService.search(this.searchTerm$)
      .subscribe(results => {
        this.mResults = results;
      });
  }

  ngOnInit() {
    this.getSkills();
  }

  getSkills() {
    this.http.get('http://localhost:5000/api/skills/').subscribe(response => {
      this.skills = response;
    }, error => {
      console.log(error);
    });
  }

  @HostListener('mousewheel')
  checkScroll() {

    const scrollPosition =
      document.querySelector('mat-sidenav-content').scrollTop ||
      0;

    console.log('[scroll]', scrollPosition);

    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  gotoTop() {
    console.log("Going to the top");
    document.querySelector('mat-sidenav-content').scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

}
